public class Duck extends Animals {
    private int age;
    private String gender;
    private String beakColor;
    public Duck() {
    }
    public Duck(int age, String gender, String beakColor) {
        this.age = age;
        this.gender = gender;
        this.beakColor = beakColor;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String getgender() {
        return gender;
    }
    public void setgender(String gender) {
        this.gender = gender;
    }
    public String getBeakColor() {
        return beakColor;
    }
    public void setBeakColor(String beakColor) {
        this.beakColor = beakColor;
    }

    @Override
    public void isMammal() {
        System.out.println("duck is not a mamal");
    }
    
    @Override
    public void mate() {
        System.out.println("duck is mating");
    }
    public void swimming() {
        System.out.println("duck is swimming...");
    }
    public void quack() {
        System.out.println("duck is quacking...");
    }

    @Override
    public String toString() {
        return "Duck[gender: " + this.gender + ", age: " + this.age + ", beak color: " + this.beakColor + "]";
    }
}
