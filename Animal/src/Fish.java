public class Fish extends Animals {
    private int age;
    private String gender;
    private int size;
    private boolean canEat;
    public Fish() {
    }
    public Fish(int age, String gender, int size, boolean canEat) {
        this.age = age;
        this.gender = gender;
        this.size = size;
        this.canEat = canEat;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public int getSize() {
        return size;
    }
    public void setSize(int size) {
        this.size = size;
    }
    public boolean isCanEat() {
        return canEat;
    }
    public void setCanEat(boolean canEat) {
        this.canEat = canEat;
    }

    @Override
    public String toString() {
        return "Fish[gender: " + this.gender + ", age: " + this.age + ", size " + this.size + "]";
    }
    @Override
    public void isMammal() {
        System.out.println("fish is not a mamal");
        
    }
    @Override
    public void mate() {
        System.out.println("duck is mating");
        
    }
}
