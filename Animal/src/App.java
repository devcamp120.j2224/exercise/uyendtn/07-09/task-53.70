public class App {
    public static void main(String[] args) throws Exception {
        Duck duck1 = new Duck(2, "Donald", "yellow");
        Fish fish1 = new Fish(3, "male", 5, true);
        Zebra zebra1 = new Zebra(1, "female", true);

        System.out.println(duck1.toString());
        System.out.println(fish1.toString());
        System.out.println(zebra1.toString());
    }
}
