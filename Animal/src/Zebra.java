public class Zebra extends Animals {
    private int age;
    private String gender;
    private boolean is_wild;
    public Zebra() {
    }
    public Zebra(int age, String gender, boolean is_wild) {
        this.age = age;
        this.gender = gender;
        this.is_wild = is_wild;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public boolean isIs_wild() {
        return is_wild;
    }
    public void setIs_wild(boolean is_wild) {
        this.is_wild = is_wild;
    }

    @Override
    public void isMammal() {
        System.out.println("zebra is a mamal");
    }

    @Override
    public void mate() {
        System.out.println("zebra is mating");
    }
    public void run() {
        System.out.println("zebra is running");
    }

    @Override
    public String toString() {
        return "Zebra[gender: " + this.gender + ", age: " + this.age + ", is wild " + this.is_wild + "]";
    }
}
